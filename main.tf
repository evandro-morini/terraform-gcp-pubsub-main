module "google-pubsub-example-topic" {
  source  = "./terraform-gcp-pubsub-topic"
  version = "0.0.1"
  topic_name = "exampleCreated"
  project_id = var.project_id

  allowed_regions = [
    "southamerica-east1",
    "us-east4",
  ]
}

module "google-pubsub-example-topic-dead-letter" {
  source  = "./terraform-gcp-pubsub-topic"
  version = "0.0.1"
  topic_name = "exampleCreated-deadletter"
  project_id = var.project_id

  allowed_regions = [
      "southamerica-east1",
      "us-east4",
    ]
}

module "google-pubsub-example-subscription" {
  source  = "./terraform-gcp-pubsub-subscription"
  version = "0.0.1"
  subscription_name = "exampleCreated-sub"
  project_id = var.project_id
  topic_id = module.google-pubsub-example-topic.google_pubsub_topic_id
  dead_letter_policy = true
  dead_letter_topic = module.google-pubsub-example-topic-dead-letter.google_pubsub_topic_id
  max_delivery_attempts = var.max_delivery_attempts
  ack_deadline_seconds = var.ack_deadline_seconds
  retain_acked_messages = true
  retry_policy = true
  minimum_backoff = var.minimum_backoff
}

module "google-pubsub-example-subscription-dead-letter" {
  source  = "./terraform-gcp-pubsub-subscription"
  version = "0.0.1"
  subscription_name = "exampleCreated-deadletter-sub"
  project_id = var.project_id
  topic_id = module.google-pubsub-example-topic-dead-letter.google_pubsub_topic_id
  ack_deadline_seconds = var.ack_deadline_seconds
  retain_acked_messages = true
}

resource "google_pubsub_topic_iam_binding" "publisher-example" {
  topic = module.google-pubsub-example-topic-dead-letter.google_pubsub_topic_name
  role         = "roles/pubsub.publisher"
  members = [
    "serviceAccount:${var.pubsub_service_account}"
  ]
}

resource "google_pubsub_subscription_iam_binding" "subscriber-example" {
  subscription = module.google-pubsub-example-subscription.google_pubsub_subscription_name
  role         = "roles/pubsub.subscriber"
  members = [
    "serviceAccount:${var.pubsub_service_account}"
  ]
}
