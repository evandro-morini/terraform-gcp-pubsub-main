variable "project_id" {
  description = "Google Cloud Project's ID"
  type        = string
}

variable "pubsub_service_account" {
  description = "Pub/Sub default service account"
  type        = string
}

variable "max_delivery_attempts" {
  description = "Pub/Sub maximum delivery attempts"
  type        = number
}

variable "ack_deadline_seconds" {
  description = "The maximum time after a subscriber receives a message before the subscriber should acknowledge the message"
  type = number
}

variable "minimum_backoff" {
  description = "The minimum delay between consecutive deliveries of a given message"
  type        = string
}

variable "region" {
  description = "Google Cloud Project Region"
  type        = string
}
