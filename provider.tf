provider "google" {
  project = var.project_id
  region = var.region
}
terraform {
  required_version = "~> 0.12.0"
}