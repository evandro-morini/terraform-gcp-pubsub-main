# Terraform GCP Infra APIGEE Pubsub

Pub/Sub infrastructure creation

### Provisioned Resources
* GCP Pub/Sub Topic
* GCP Pub/Sub Subscription
* GCP Pub/Sub Topic IAM Biding
* GCP Pub/Sub Subscription IAM Biding

### Customized variables 
* region = Google Cloud Project Region
* Project ID = Google Cloud Project's ID
* pubsub_service_account = Pub/Sub default service account
* max_delivery_attempts = Pub/Sub maximum delivery attempts
* ack_deadline_seconds = Maximum time after a subscriber receives a message before the subscriber should acknowledge the message
* minimum_backoff = The minimum delay between consecutive deliveries of a given message

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| google | n/a |

## Outputs

| Name | Description |
|------|-------------|
| google-pubsub-example-topic-id | Example Created Topic ID |
| google-pubsub-example-subscription-id | Example Created Subscription ID |