output "google-pubsub-example-topic-id" {
  description = "Example Created Topic ID"
  value       = "${module.google-pubsub-example-topic.google_pubsub_topic_id}"
}

output "google-pubsub-example-subscription-id" {
  description = "Example Created Subscription ID"
  value       = "${module.google-pubsub-example-subscription.google_pubsub_subscription_id}"
}
